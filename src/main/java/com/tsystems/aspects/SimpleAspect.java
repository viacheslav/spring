package com.tsystems.aspects;


import com.tsystems.model.Book;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class SimpleAspect {

    @AfterReturning(value = "execution(* com.tsystems.services.BookService.*(..))", returning = "serviceResult")
    public Book changeBookName(Book serviceResult) {
        System.out.println("BookService return result - " + serviceResult);
        serviceResult.setName(serviceResult.getName() + ": after aspect");
        return serviceResult;
    }

    @AfterThrowing(pointcut = "execution(* com.tsystems.services.BookService.*(..))",throwing = "nullPointEx")
    public void afterThrowingAdvice(JoinPoint jp,NullPointerException nullPointEx){
        System.out.println("Exception is "+nullPointEx.getLocalizedMessage());
        System.out.println("Annotation driven:After throwing " + jp.getSignature().getName() + "()");
    }

    @Around("execution(* com.tsystems.services.BookService.*(..))")
    public Object beforeAdvice(ProceedingJoinPoint jp) throws Throwable{
        System.out.println("Annotation driven:Around advice");
        Object[] args=jp.getArgs();
        if(args.length>0){
            System.out.print("Arguments passed:");
            for (int i = 0; i < args.length; i++) {
                System.out.println("Arg"+(i+1)+":"+args[i]);
                //args[i]=":Annotation driven argument";
            }
        }
        return jp.proceed(args);
    }
}
