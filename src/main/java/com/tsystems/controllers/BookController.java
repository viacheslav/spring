package com.tsystems.controllers;

import com.tsystems.model.Book;
import com.tsystems.services.BookService;
import com.tsystems.validators.PriceValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Iterator;


@Controller
@RequestMapping("/editBook")
public class BookController extends MainController {
    @Autowired
    private BookService bookService;

    @Autowired
    private PriceValidator priceValidator;

    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(priceValidator);
    }

    @RequestMapping("/{id}")
    public String findBook(@PathVariable long id, ModelMap model) {
        model.addAttribute("book", bookService.findBook(id));
        return "editBook";
    }

    @RequestMapping("/find")
    public String findBookByName(ModelMap model, HttpServletRequest request) {
        Book foundBook = bookService.findBookByName(request.getParameter("name"));
        if (foundBook == null) {
            return "bookNotFound";
        }
        model.addAttribute("book", foundBook);
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Iterator<? extends GrantedAuthority> iterator = userDetails.getAuthorities().iterator();
        while (iterator.hasNext()) {
            GrantedAuthority authority = iterator.next();
            if (authority.getAuthority().equals("ROLE_ADMIN")) {
                return "editBook";
            }
        }
        return "book";
    }

    @RequestMapping("/update")
    public String editBook(HttpServletRequest request, ModelMap model) {
        Book book = new Book();
        book.setName(request.getParameter("name"));
        book.setPrice(Integer.parseInt(request.getParameter("price")));
        book.setId(Long.parseLong(request.getParameter("id")));
        bookService.update(book);

        return showMainPage(model);
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView createBook() {
        ModelAndView modelAndView=new ModelAndView("createBook");
        modelAndView.addObject("book",new Book());
        return modelAndView;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createBook(HttpServletRequest request, ModelMap model) {
        Book book = new Book();
        book.setName(request.getParameter("name"));
        book.setPrice(Integer.parseInt(request.getParameter("price")));
        bookService.save(book);

        return showMainPage(model);
    }

    @RequestMapping(value = "/createBook", method = RequestMethod.POST)
    public String createBook2(HttpServletRequest request,@ModelAttribute @Valid Book book, ModelMap model) {
        book.setName(request.getParameter("name"));
        book.setPrice(Integer.parseInt(request.getParameter("price")));
        bookService.save(book);
        return showMainPage(model);
    }
}
