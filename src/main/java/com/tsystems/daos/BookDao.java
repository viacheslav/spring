package com.tsystems.daos;


import com.tsystems.model.Book;

import java.util.List;


public interface BookDao {
    List<Book> getAllBooksAbovePrice(int price);
    List<Book> getAllBooks();
    Book findBook(Long id);

    void addBook(Book book);

    void deleteBook(Book book);

    void updateBook(Book book);

    Book findBookByName(String name);
}
