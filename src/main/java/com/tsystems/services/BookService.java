package com.tsystems.services;

import com.tsystems.model.Book;

import java.util.List;


public interface BookService {
    List<Book> getAllBooks();

    Book findBook(long id);

    void delete(long id);

    void update(Book book);

    void save(Book book);

    Book findBookByName(String name);
}
