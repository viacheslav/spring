package com.tsystems.services;

import com.tsystems.daos.BookDao;
import com.tsystems.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BookServiceImpl implements BookService {
    @Autowired
    private BookDao dao;

    @Override
    public List<Book> getAllBooks() {
        return dao.getAllBooks();
    }

    @Override
    public Book findBook(long id) {
        return dao.findBook(id);
    }

    @Override
    public void delete(long id) {
        dao.deleteBook(findBook(id));
    }

    @Override
    public void update(Book book) {
        dao.updateBook(book);
    }

    @Override
    public void save(Book book) {
        dao.addBook(book);
    }

    @Override
    public Book findBookByName(String name) {
        return dao.findBookByName(name);
    }
}
