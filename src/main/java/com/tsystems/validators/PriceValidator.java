package com.tsystems.validators;

import com.tsystems.model.Book;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


@Component
public class PriceValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(Book.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Book book=(Book)o;
        if (((Book) o).getPrice()<10) {
            errors.reject("price","incorrect");
        }
    }
}
