<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<body>
<form action="create" method="POST">
    Book Name: <input type="text" name="name" >
    <br />
    price: <input type="text" name="price"/>
    <input type="submit" value="Submit" />
</form>

Form with validator:
<form:form modelAttribute="book" action="createBook" method="POST">
    Book Name: <form:input type="text" path="name" />
    <br />
    price: <form:input type="text" path="price"/>
    <form:errors path="price" cssstyle="color: red;"/>
    <form:button type="submit" value="Submit">Submit</form:button>
</form:form>
</body>
</html>